/*
 * ei-bot
 * Copyright (C) 2021  TechnoElf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#![feature(half_open_range_patterns)]
#![feature(exclusive_range_pattern)]

use std::time::Duration;
use std::collections::{HashMap, HashSet};
use std::panic;
use std::fmt::Display;

use chrono::{Utc, Datelike};

use serenity::prelude::*;
use serenity::model::prelude::*;
use serenity::async_trait;
use serenity::client::bridge::gateway::GatewayIntents;
use serenity::utils::Colour;
use serenity::builder::{EditRole, EditChannel, CreateChannel};

use tokio::time;

mod db;
use db::{Db, User, Module, Semester, Level};

const SEMESTERS: u32 = 10;
const SEM_GRAD_START: Colour = Colour::BLITZ_BLUE;
const SEM_GRAD_END: Colour = Colour::DARK_TEAL;
const GUILD: u64 = 893940743721324544;
const STDOUT_CHANNEL: u64 = 884371255376105482;

struct Handler {
    db: Mutex<Db>,
    guild_id: GuildId,
    semester_channels: Mutex<HashMap<u32, ChannelId>>,
    joined_channel: Mutex<Option<ChannelId>>,
    semester_roles: Mutex<HashMap<u32, RoleId>>,
    module_roles: Mutex<HashMap<String, RoleId>>,
    other_roles: Mutex<HashMap<String, RoleId>>,
    joined_role: Mutex<Option<RoleId>>,
    member_role: Mutex<Option<RoleId>>,
    everyone_role: Mutex<Option<RoleId>>,
    running: Mutex<bool>
}

#[async_trait]
impl EventHandler for Handler {
    async fn message(&self, mut ctx: Context, msg: Message) {
        if msg.author == ctx.http.get_current_user().await.unwrap().into() || msg.guild_id != Some(self.guild_id) { return; }
        let channel = ctx.http.get_channel(msg.channel_id.0).await.unwrap().guild().unwrap();
        if !(&channel.name == "🤖-bot-befehle" || &channel.name == "👋-neue-mitglieder") { return; }

        if msg.content.starts_with(".") {
            let cmd: Vec<&str> = msg.content[1..].split_whitespace().collect();
            match cmd[0] {
                "beitreten" => {
                    self.db.lock().await.write_user(User {
                        id: msg.author.id,
                        semester: Semester::None,
                        modules: HashSet::new(),
                        roles: HashSet::new()
                    });
                    
                    let m = msg.guild_id.unwrap().member(&ctx.http, msg.author).await.unwrap();
                    self.assign_roles_for_user(&mut ctx, m).await;
                },
                "semester" | "sem" => {
                    let mut user: u64 = msg.author.id.0;
                    let mut semester: i32 = 0;

                    for arg in cmd[1..].iter() {
                        if arg.starts_with("<@!") {
                            user = arg[3..(arg.len() - 1)].parse().unwrap()
                        } else {
                            semester = arg.parse().unwrap()
                        }
                    }

                    let u = self.db.lock().await.get_user_by_id(user);
                    if let Some(mut u) = u {
                        u.semester = Semester::from(semester);
                        self.db.lock().await.write_user(u);
                        let m = msg.guild_id.unwrap().member(&ctx.http, msg.author).await.unwrap();
                        self.assign_roles_for_user(&mut ctx, m).await;

                        msg.channel_id.send_message(&ctx.http, |m| m.embed(|e| {
                            e.title("Semester geändert").colour(Colour::FOOYOO)
                        })).await.unwrap();
                    }
                },
                "modul" | "mod" => {
                    if cmd.len() >= 2 && (cmd[1] == "hinzufügen" || cmd[1] == "bearbeiten") {
                        if self.db.lock().await.get_user_max_level(msg.author.id.0) >= Level::Mod {
                            self.db.lock().await.write_module(Module {
                                name: cmd[2].to_lowercase(),
                                semester: Semester::from(cmd[3].parse().unwrap()),
                                display: cmd[4].to_string()
                            });
                            self.ensure_roles(&mut ctx).await;
                            self.ensure_channels(&mut ctx).await;
                        } else {
                            msg.channel_id.send_message(&ctx.http, |m| m.embed(|e| {
                                e.title("Nicht berechtigt").colour(Colour::RED)
                            })).await.unwrap();
                        }
                    } else if cmd.len() >= 2 && cmd[1] == "löschen" {
                        if self.db.lock().await.get_user_max_level(msg.author.id.0) >= Level::Mod {
                            self.guild_id.delete_role(&ctx.http, self.module_roles.lock().await[cmd[2]]).await.unwrap();
                            self.module_roles.lock().await.remove(cmd[2]);
                            self.db.lock().await.delete_module_by_name(cmd[2]);
                        } else {
                            msg.channel_id.send_message(&ctx.http, |m| m.embed(|e| {
                                e.title("Nicht berechtigt").colour(Colour::RED)
                            })).await.unwrap();
                        }
                    } else if cmd.len() == 1 {
                        let modules = self.db.lock().await.get_modules();
                        let modules = modules.into_iter().map(|r| r.name).collect::<Vec<String>>().join("\n");
                        msg.channel_id.send_message(&ctx.http, |m| m.embed(|e| {
                            e.title("Verfügbare Module:").colour(Colour::FOOYOO).description(modules)
                        })).await.unwrap();
                    } else {
                        let mut user: u64 = msg.author.id.0;
                        let mut leave: bool = false;
                        let mut module: Option<String> = None;

                        for arg in cmd[1..].iter() {
                            if arg == &"beitreten" {
                                leave = false
                            } else if arg == &"austreten" {
                                leave = true
                            } else if arg.starts_with("<@!") {
                                user = arg[3..(arg.len() - 1)].parse().unwrap()
                            } else {
                                module = Some(arg.to_lowercase())
                            }
                        }

                        if let Some(module) = module {
                            let m = self.db.lock().await.get_module_by_name(&module).unwrap();
                            let u = self.db.lock().await.get_user_by_id(user);
                            if let Some(mut u) = u {
                                if leave {
                                    u.modules.remove(&m.name);
                                    msg.channel_id.send_message(&ctx.http, |m| m.embed(|e| {
                                        e.title("Modul ausgetreten").colour(Colour::FOOYOO)
                                    })).await.unwrap();
                                } else {
                                    u.modules.insert(m.name);
                                    msg.channel_id.send_message(&ctx.http, |m| m.embed(|e| {
                                        e.title("Modul beigetreten").colour(Colour::FOOYOO)
                                    })).await.unwrap();
                                }
                                let m = msg.guild_id.unwrap().member(&ctx.http, u.id).await.unwrap();
                                self.db.lock().await.write_user(u);
                                self.assign_roles_for_user(&mut ctx, m).await;
                            }
                        } else {
                            msg.channel_id.send_message(&ctx.http, |m| m.embed(|e| {
                                e.title("Befehl nicht erkannt").colour(Colour::RED)
                            })).await.unwrap();
                        }
                    }
                },
                "rolle" | "rol" => {
                    if cmd.len() >= 2 && (cmd[1] == "hinzufügen" || cmd[1] == "bearbeiten") {
                        if self.db.lock().await.get_user_max_level(msg.author.id.0) >= Level::Admin {
                            self.db.lock().await.write_role(db::Role {
                                name: cmd[2].to_lowercase(),
                                colour: Colour::new(u32::from_str_radix(cmd[3], 16).unwrap()),
                                level: Level::from(cmd[4].parse().unwrap()),
                                permissions: Permissions::from_bits(cmd[5].parse().unwrap()).unwrap()
                            });
                            self.ensure_roles(&mut ctx).await;
                        } else {
                            msg.channel_id.send_message(&ctx.http, |m| m.embed(|e| {
                                e.title("Nicht berechtigt").colour(Colour::RED)
                            })).await.unwrap();
                        }
                    } else if cmd.len() >= 2 && cmd[1] == "löschen" {
                        if self.db.lock().await.get_user_max_level(msg.author.id.0) >= Level::Admin {
                            self.guild_id.delete_role(&ctx.http, self.other_roles.lock().await[cmd[2]]).await.unwrap();
                            self.other_roles.lock().await.remove(cmd[2]);
                            self.db.lock().await.delete_role_by_name(cmd[2]);
                        } else {
                            msg.channel_id.send_message(&ctx.http, |m| m.embed(|e| {
                                e.title("Nicht berechtigt").colour(Colour::RED)
                            })).await.unwrap();
                        }
                    } else if cmd.len() == 1 {
                        let roles = self.db.lock().await.get_roles();
                        let roles = roles.into_iter().map(|r| r.name).collect::<Vec<String>>().join("\n");
                        msg.channel_id.send_message(&ctx.http, |m| m.embed(|e| {
                            e.title("Verfügbare Rollen:").colour(Colour::FOOYOO).description(roles)
                        })).await.unwrap();
                    } else {
                        let mut user: u64 = msg.author.id.0;
                        let mut leave: bool = false;
                        let mut role: Option<String> = None;

                        for arg in cmd[1..].iter() {
                            if arg == &"zuweisen" {
                                leave = false
                            } else if arg == &"entfernen" {
                                leave = true
                            } else if arg.starts_with("<@!") {
                                user = arg[3..(arg.len() - 1)].parse().unwrap()
                            } else {
                                role = Some(arg.to_lowercase())
                            }
                        }

                        if let Some(role) = role {
                            let r = self.db.lock().await.get_role_by_name(&role).unwrap();
                            let u = self.db.lock().await.get_user_by_id(user);
                            if let Some(mut u) = u {
                                if self.db.lock().await.get_user_max_level(msg.author.id.0) >= r.level {
                                    if leave {
                                        u.roles.remove(&r.name);
                                        msg.channel_id.send_message(&ctx.http, |m| m.embed(|e| {
                                            e.title("Rolle entfernt").colour(Colour::FOOYOO)
                                        })).await.unwrap();
                                    } else {
                                        u.roles.insert(r.name);
                                        msg.channel_id.send_message(&ctx.http, |m| m.embed(|e| {
                                            e.title("Rolle zugewiesen").colour(Colour::FOOYOO)
                                        })).await.unwrap();
                                    }
                                    let m = msg.guild_id.unwrap().member(&ctx.http, u.id).await.unwrap();
                                    self.db.lock().await.write_user(u);
                                    self.assign_roles_for_user(&mut ctx, m).await;
                                } else {
                                    msg.channel_id.send_message(&ctx.http, |m| m.embed(|e| {
                                        e.title("Nicht berechtigt").colour(Colour::RED)
                                    })).await.unwrap();
                                }
                            }
                        } else {
                            msg.channel_id.send_message(&ctx.http, |m| m.embed(|e| {
                                e.title("Befehl nicht erkannt").colour(Colour::RED)
                            })).await.unwrap();
                        }
                    }
                },
                "ban" => {
                    if cmd.len() == 2 && cmd[1].starts_with("<@!") {
                        if self.db.lock().await.get_user_max_level(msg.author.id.0) >= Level::Mod {
                            let u = UserId(cmd[1][3..(cmd[1].len() - 1)].parse().unwrap());
                            let m = msg.guild_id.unwrap().member(&ctx.http, u).await.unwrap();
                            m.ban(&ctx.http, 0).await.unwrap();
                            msg.channel_id.send_message(&ctx.http, |m| m.embed(|e| {
                                e.title("Benutzer gebanned").colour(Colour::FOOYOO)
                            })).await.unwrap();
                        } else {
                            msg.channel_id.send_message(&ctx.http, |m| m.embed(|e| {
                                e.title("Nicht berechtigt").colour(Colour::RED)
                            })).await.unwrap();
                        }
                    } else {
                        msg.channel_id.send_message(&ctx.http, |m| m.embed(|e| {
                            e.title("Befehl nicht erkannt").colour(Colour::RED)
                        })).await.unwrap();
                    }
                },
                "hilfe" => {
                    msg.channel_id.send_message(&ctx.http, |m| m.embed(|e| {
                        e.title("Verfügbare Befehle").colour(Colour::FOOYOO).description("\
                            .hilfe\n\
                            .semester/.sem (semester)\n\
                            .modul/.mod\n\
                            .modul/.mod (modul) [beitreten/austreten] [benutzer]\n\
                            .modul/.mod hinzufügen/bearbeiten/löschen (modul) (semester) (channel)\n\
                            .rolle/.rol\n\
                            .rolle/.rol (rolle) [zuweisen/entfernen] [benutzer]\n\
                            .rolle/.rol hinzufügen/bearbeiten/löschen (rolle) (farbe) (level) (permissions)\n\
                            .ban (benutzer)\n\
                        ")
                    })).await.unwrap();
                },
                _ => {
                    msg.channel_id.send_message(&ctx.http, |m| m.embed(|e| {
                        e.title("Befehl nicht erkannt").colour(Colour::RED).description("\".hilfe\" zeigt die verfügbaren Befehle an")
                    })).await.unwrap();
                }
            }
        }
    }

    async fn guild_member_addition(&self, ctx: Context, guild: GuildId, mut member: Member) {
        if guild != self.guild_id { return; }

        member.add_role(&ctx.http, self.joined_role.lock().await.unwrap()).await.unwrap();

        let msg = "
            🇩🇪
            Willkommen auf dem Discord-Server für die Studierenden der Elektrotechnik und Informationstechnik an der TU München! 

            Hier habt ihr die Möglichkeit eure Kommilitonen kennenzulernen, miteinander zu lernen, Sport zu treiben, sonstige Projekte zu starten - aber auch Fragen zum Studium zu stellen.
            Bevor ihr Zugang zum Server erhaltet, lest euch bitte zunächst die 📔-regeln durch und akzeptiert sie, indem du \".beitreten\" im Kanal #👋-neue-mitglieder schreibst.

            Viel Spaß und werdet aktiv!
            Eure Admins und Fachschaft

            🇬🇧
            Welcome to the discord server for the students of Electrical and Computer Engineering in the 1st until 4th semester at the TU Munich!

            Here, you'll have the chance to meet your fellow students, study with each other, meet up for sports or other projects - but also to pose questions about your studies.

            Before you'll get access to the server, please read through 📔-regeln and accept them with by sending \".beitreten\" in the channel #👋-neue-mitglieder.

            Have fun and get active!
            Your admins and student council 
        ";

        member.user.create_dm_channel(&ctx.http).await.unwrap().send_message(&ctx.http, |m| m.embed(|e| {
            e.title("Willkommen im TUM EI Server!").colour(Colour::MAGENTA).description(msg)
        })).await.unwrap();


        let channel = self.joined_channel.lock().await.clone();
        tokio::spawn(async move {
            time::sleep(Duration::from_secs(2)).await;
            channel.unwrap().send_message(&ctx.http, |m| m.embed(|e| {
                e.title("Willkommen im TUM EI Server!").colour(Colour::MAGENTA).description(msg)
            })).await.unwrap();
        });
    }

    async fn guild_member_removal(&self, _ctx: Context, guild: GuildId, user: serenity::model::user::User, _member: Option<Member>) {
        if guild != self.guild_id { return; }
        self.db.lock().await.delete_user_by_id(user.id.0);
    }

    async fn ready(&self, ctx: Context, _: Ready) {
        let execute = {
            let r = &mut *self.running.lock().await;
            if !*r {
                *r = true;
                true
            } else {
                false
            }
        };

        if execute {
            let c = ctx.clone();
            panic::set_hook(Box::new(move |e| {
                stdout(&c, e);
                eprintln!("{}", e);
            }));

            self.background_tasks(ctx).await;
        }
    }
}

impl Handler {
    async fn background_tasks(&self, mut ctx: Context) {
        eprintln!("Performing initial tasks...");
        stdout(&ctx, "Performing initial tasks...");
        self.initial_tasks(&mut ctx).await;

        let mut delay = time::interval(Duration::from_secs(3600));
        delay.tick().await;
        loop {
            let today = Utc::today();
            if self.db.lock().await.get_date() != today {
                self.db.lock().await.update_date();

                if today.day() == 1 && (today.month() == 4 || today.month() == 10) {
                    eprintln!("Performing semesterly tasks...");
                    stdout(&ctx, "Performing semesterly tasks...");
                    self.semesterly_tasks(&mut ctx).await;
                }

                eprintln!("Performing daily tasks...");
                stdout(&ctx, "Performing daily tasks...");
                self.daily_tasks(&mut ctx).await;
            }

            eprintln!("Performing hourly tasks...");
            stdout(&ctx, "Performing hourly tasks...");
            self.hourly_tasks(&mut ctx).await;

            eprintln!("Done.");
            stdout(&ctx, "Done.");
            delay.tick().await;
        }
    }

    async fn initial_tasks(&self, ctx: &mut Context) {
        self.ensure_roles(ctx).await;
        self.ensure_channels(ctx).await;
        self.assign_roles(ctx).await;
    }

    async fn hourly_tasks(&self, _ctx: &mut Context) {

    }

    async fn daily_tasks(&self, ctx: &mut Context) {
        self.ensure_roles(ctx).await;
        self.ensure_channels(ctx).await;
        self.assign_roles(ctx).await;
    }

    async fn semesterly_tasks(&self, _ctx: &mut Context) {

    }

    async fn ensure_roles(&self, ctx: &mut Context) {
        let roles: HashMap<String, (RoleId, Role)> = self.guild_id.roles(&ctx.http).await.unwrap().drain().map(|(r_id, r)| (r.name.clone(), (r_id, r))).collect();

        for sem in 1..=SEMESTERS {
            self.semester_roles.lock().await.insert(sem - 1, self.ensure_role(ctx, RoleConf {
                colour: {
                    let prop = sem as f32 / ((SEMESTERS + 1) as f32);
                    let r = (SEM_GRAD_START.r() as f32 * (1.0 - prop) + SEM_GRAD_END.r() as f32 * prop) as u8;
                    let g = (SEM_GRAD_START.g() as f32 * (1.0 - prop) + SEM_GRAD_END.g() as f32 * prop) as u8;
                    let b = (SEM_GRAD_START.b() as f32 * (1.0 - prop) + SEM_GRAD_END.b() as f32 * prop) as u8;
                    Colour::from_rgb(r, g, b)
                },
                hoist: true,
                name: format!("{}. Semester", sem),
                permissions: Permissions::empty()
            }, &roles).await);
        }

        self.semester_roles.lock().await.insert(SEMESTERS, self.ensure_role(ctx, RoleConf {
            colour: SEM_GRAD_END,
            hoist: true,
            name: "Alumni".to_string(),
            permissions: Permissions::empty()
        }, &roles).await);

        *self.joined_role.lock().await = Some(self.ensure_role(ctx, RoleConf {
            colour: Colour::LIGHT_GREY,
            hoist: false,
            name: "neues-mitglied".to_string(),
            permissions: Permissions::empty()
        }, &roles).await);

        *self.member_role.lock().await = Some(self.ensure_role(ctx, RoleConf {
            colour: Colour::LIGHT_GREY,
            hoist: false,
            name: "mitglied".to_string(),
            permissions: Permissions::CHANGE_NICKNAME | Permissions::SPEAK | Permissions::READ_MESSAGE_HISTORY | Permissions::ATTACH_FILES | Permissions::EMBED_LINKS | Permissions::ADD_REACTIONS | Permissions::CREATE_INVITE
        }, &roles).await);

        *self.everyone_role.lock().await = Some(self.ensure_role(ctx, RoleConf {
            colour: Colour::LIGHT_GREY,
            hoist: false,
            name: "@everyone".to_string(),
            permissions: Permissions::empty()
        }, &roles).await);

        for module in self.db.lock().await.get_modules() {
            self.module_roles.lock().await.insert(module.name.clone(), self.ensure_role(ctx, RoleConf {
                colour: Colour::ROSEWATER,
                hoist: false,
                name: module.name,
                permissions: Permissions::empty()
            }, &roles).await);
        }

        for role in self.db.lock().await.get_roles() {
            self.other_roles.lock().await.insert(role.name.clone(), self.ensure_role(ctx, RoleConf {
                colour: role.colour,
                hoist: true,
                name: role.name,
                permissions: role.permissions
            }, &roles).await);
        }
    }

    async fn ensure_role(&self, ctx: &mut Context, r: RoleConf, roles: &HashMap<String, (RoleId, Role)>) -> RoleId {
        if !roles.contains_key(&r.name) {
            self.guild_id.create_role(&ctx.http, r.as_edit()).await.unwrap().id
        } else if r != roles[&r.name].1 {
            roles[&r.name].1.edit(&ctx.http, r.as_edit()).await.unwrap().id
        } else {
            roles[&r.name].0
        }
    }

    async fn assign_roles(&self, ctx: &mut Context) {
        let members = self.guild_id.members(&ctx.http, None, None).await.unwrap();
        for m in members {
            if !m.user.bot {
                self.assign_roles_for_user(ctx, m).await;
            }
        }
    }

    async fn assign_roles_for_user(&self, ctx: &mut Context, mut m: Member) {
        let user = self.db.lock().await.get_user_by_id(m.user.id.0);
        if let Some(user) = user {
            m.remove_roles(&ctx.http, &m.roles.clone()).await;
            m.add_role(&ctx.http, self.member_role.lock().await.unwrap()).await.unwrap();

            match user.semester {
                Semester::None => (),
                Semester::Alumni => { m.add_role(&ctx.http, self.semester_roles.lock().await[&SEMESTERS]).await.unwrap(); }
                Semester::Sem(s) => { m.add_role(&ctx.http, self.semester_roles.lock().await[&(s - 1)]).await.unwrap(); }
            }

            for module in &user.modules {
                if let Some(r) = self.module_roles.lock().await.get(module) {
                    m.add_role(&ctx.http, r).await.unwrap();
                } else {
                    let mut u = user.clone();
                    u.modules.remove(module);
                    self.db.lock().await.write_user(u);
                }
            }

            for role in &user.roles {
                if let Some(r) = self.other_roles.lock().await.get(role) {
                    m.add_role(&ctx.http, r).await.unwrap();
                } else {
                    let mut u = user.clone();
                    u.roles.remove(role);
                    self.db.lock().await.write_user(u);
                }
            }
        } else {
            m.remove_roles(&ctx.http, &m.roles.clone()).await.unwrap();
            m.add_role(&ctx.http, self.joined_role.lock().await.unwrap()).await.unwrap();
        }
    }

    async fn ensure_channels(&self, ctx: &mut Context) {
        let mut channels: HashMap<String, (ChannelId, GuildChannel)> = self.guild_id.channels(&ctx.http).await.unwrap().drain().map(|(c_id, c)| (c.name.clone(), (c_id, c))).collect();
        
        for sem in 1..=SEMESTERS {
            let cat = self.ensure_channel(ctx, ChannelConf {
                name: format!("{}. Semester", sem),
                kind: ChannelType::Category,
                category: None,
                permissions: Vec::new()
            }, &mut channels).await;

            let sem_name = vec!["", "ersti", "zweiti", "dritti", "vierti", "fünfti", "sechsti", "siebti", "achti", "neunti", "zehnti"];

            self.ensure_channel(ctx, ChannelConf {
                name: format!("🌳-{}-buschfunk", sem_name[sem as usize]),
                kind: ChannelType::Text,
                category: Some(cat),
                permissions: vec![PermissionOverwrite {
                    allow: Permissions::READ_MESSAGES | Permissions::SEND_MESSAGES,
                    deny: Permissions::empty(),
                    kind: PermissionOverwriteType::Role(self.semester_roles.lock().await[&(sem - 1)])
                }]
            }, &mut channels).await;

            self.semester_channels.lock().await.insert(sem, cat);
        }

        let other_cat = self.ensure_channel(ctx, ChannelConf {
            name: "Wahlmodule".to_string(),
            kind: ChannelType::Category,
            category: None,
            permissions: Vec::new()
        }, &mut channels).await;

        let welcome_cat = self.ensure_channel(ctx, ChannelConf {
            name: "Willkommen".to_string(),
            kind: ChannelType::Category,
            category: None,
            permissions: Vec::new()
        }, &mut channels).await;

        *self.joined_channel.lock().await = Some(self.ensure_channel(ctx, ChannelConf {
            name: "👋-neue-mitglieder".to_string(),
            kind: ChannelType::Text,
            category: Some(welcome_cat),
            permissions: vec![PermissionOverwrite {
                allow: Permissions::READ_MESSAGES | Permissions::SEND_MESSAGES,
                deny: Permissions::empty(),
                kind: PermissionOverwriteType::Role(self.joined_role.lock().await.unwrap())
            }]
        }, &mut channels).await);

        self.ensure_channel(ctx, ChannelConf {
            name: "⛑-support".to_string(),
            kind: ChannelType::Text,
            category: Some(welcome_cat),
            permissions: vec![PermissionOverwrite {
                allow: Permissions::READ_MESSAGES | Permissions::SEND_MESSAGES,
                deny: Permissions::empty(),
                kind: PermissionOverwriteType::Role(self.everyone_role.lock().await.unwrap())
            }]
        }, &mut channels).await;

        self.ensure_channel(ctx, ChannelConf {
            name: "🤖-bot-befehle".to_string(),
            kind: ChannelType::Text,
            category: Some(welcome_cat),
            permissions: vec![PermissionOverwrite {
                allow: Permissions::READ_MESSAGES | Permissions::SEND_MESSAGES,
                deny: Permissions::empty(),
                kind: PermissionOverwriteType::Role(self.member_role.lock().await.unwrap())
            }]
        }, &mut channels).await;

        let modules = self.db.lock().await.get_modules();
        for module in modules {
            self.ensure_channel(ctx, ChannelConf {
                name: module.display.clone(),
                kind: ChannelType::Text,
                category: if let Semester::Sem(s) = module.semester { Some(self.semester_channels.lock().await[&s]) } else { Some(other_cat) },
                permissions: vec![PermissionOverwrite {
                    allow: Permissions::READ_MESSAGES | Permissions::SEND_MESSAGES,
                    deny: Permissions::empty(),
                    kind: PermissionOverwriteType::Role(self.module_roles.lock().await[&module.name])
                }]
            }, &mut channels).await;
        }
    }

    async fn ensure_channel(&self, ctx: &mut Context, c: ChannelConf, channels: &mut HashMap<String, (ChannelId, GuildChannel)>) -> ChannelId {
        if !channels.contains_key(&c.name) {
            self.guild_id.create_channel(&ctx.http, c.as_create()).await.unwrap().id
        } else if c != channels[&c.name].1 {
            channels.get_mut(&c.name).unwrap().1.edit(&ctx.http, c.as_edit()).await.unwrap();
            channels[&c.name].0
        } else {
            channels[&c.name].0
        }
    }
}

fn stdout<T: Display>(ctx: &Context, s: T) {
    let ctx = ctx.clone();
    let mut msg = format!("{}", s).replace("`", "''");
    msg.insert(0, '`');
    msg.push('`');
    tokio::spawn(async move {
        ctx.http.get_channel(STDOUT_CHANNEL).await.unwrap().id().send_message(ctx.http, |m| {
            m.content(msg)
        }).await.unwrap();
    });
}

#[derive(Debug, Clone)]
struct RoleConf {
    pub name: String,
    pub colour: Colour,
    pub hoist: bool,
    pub permissions: Permissions
}

impl RoleConf {
    pub fn as_edit(&self) -> impl FnOnce(&mut EditRole) -> &mut EditRole {
        let s = self.clone();
        |r| {
            r.name(s.name);
            r.colour(s.colour.0 as u64);
            r.hoist(s.hoist);
            r.permissions(s.permissions);
            r
        }
    }
}

impl PartialEq<Role> for RoleConf {
    fn eq(&self, other: &Role) -> bool {
        self.colour == other.colour && self.hoist == other.hoist && self.permissions == other.permissions
    }
}

#[derive(Debug, Clone)]
struct ChannelConf {
    pub name: String,
    pub kind: ChannelType,
    pub category: Option<ChannelId>,
    pub permissions: Vec<PermissionOverwrite>
}

impl ChannelConf {
    pub fn as_edit(&self) -> impl FnOnce(&mut EditChannel) -> &mut EditChannel {
        let s = self.clone();
        |c| {
            c.name(s.name);
            c.permissions(s.permissions);
            c.category(s.category);
            c
        }
    }

    pub fn as_create(&self) -> impl FnOnce(&mut CreateChannel) -> &mut CreateChannel {
        let s = self.clone();
        |c| {
            c.name(s.name);
            c.kind(s.kind);
            c.permissions(s.permissions);
            if let Some(cat) = s.category { c.category(cat); }
            c
        }
    }
}

impl PartialEq<GuildChannel> for ChannelConf {
    fn eq(&self, other: &GuildChannel) -> bool {
        for perm in &self.permissions {
            if let Some(other_perm) = other.permission_overwrites.iter().find(|p| p.kind == perm.kind) {
                if other_perm.allow != perm.allow || other_perm.deny != perm.deny {
                    return false;
                }
            } else {
                return false;
            }
        }

        self.category == other.category_id
    }
}

#[tokio::main]
async fn main() {
    let mut settings = config::Config::default();
    settings.merge(config::File::with_name("conf")).unwrap();

    let mut client = Client::builder(settings.get_str("token").unwrap()).event_handler(Handler {
        db: Mutex::new(Db::init()),
        guild_id: GuildId(GUILD),
        semester_channels: Mutex::new(HashMap::new()),
        joined_channel: Mutex::new(None),
        semester_roles: Mutex::new(HashMap::new()),
        module_roles: Mutex::new(HashMap::new()),
        other_roles: Mutex::new(HashMap::new()),
        joined_role: Mutex::new(None),
        member_role: Mutex::new(None),
        everyone_role: Mutex::new(None),
        running: Mutex::new(false)
    }).intents(GatewayIntents::non_privileged() | GatewayIntents::GUILD_MEMBERS).await.unwrap();
    client.start().await.unwrap();
}
