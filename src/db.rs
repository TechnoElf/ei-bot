/*
 * ei-bot
 * Copyright (C) 2021  TechnoElf
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use std::collections::HashSet;
use std::cmp::Ordering;

use chrono::{Utc, Date, NaiveDate};

use rusqlite::{Connection, params};

use serenity::utils::Colour;
use serenity::model::prelude::*;

pub struct Db {
    con: Connection
}

impl Db {
    pub fn init() -> Self {
        let con = Connection::open("bot.db").unwrap();

        con.execute("CREATE TABLE IF NOT EXISTS users (id INTEGER NOT NULL PRIMARY KEY, semester INTEGER NOT NULL, modules TEXT NOT NULL, roles TEXT NOT NULL)", []).unwrap();
        con.execute("CREATE TABLE IF NOT EXISTS modules (name TEXT NOT NULL PRIMARY KEY, semester INTEGER NOT NULL, display TEXT NOT NULL)", []).unwrap();
        con.execute("CREATE TABLE IF NOT EXISTS roles (name TEXT NOT NULL PRIMARY KEY, colour INTEGER NOT NULL, level INTEGER NOT NULL, permissions INTEGER NOT NULL)", []).unwrap();
        con.execute("CREATE TABLE IF NOT EXISTS state (key TEXT NOT NULL PRIMARY KEY, value TEXT NOT NULL)", []).unwrap();

        Self {
            con
        }
    }

    pub fn get_user_by_id(&mut self, u: u64) -> Option<User> {
        self.con.query_row("SELECT id, semester, modules, roles FROM users WHERE id = ?", [u], |row| Ok(User {
            id: UserId(row.get(0)?),
            semester: Semester::from(row.get(1)?),
            modules: row.get::<_, String>(2)?.split_terminator(";").map(|v| v.to_string()).collect(),
            roles: row.get::<_, String>(3)?.split_terminator(";").map(|v| v.to_string()).collect()
        })).ok()
    }

    pub fn delete_user_by_id(&mut self, u: u64) -> bool {
        self.con.execute("DELETE FROM users WHERE id = ?", [u]).is_ok()
    }

    pub fn write_user(&mut self, u: User) {
        if self.con.query_row("SELECT 0 FROM users WHERE id = ?", [u.id.0], |_| Ok(())).is_ok() {
            self.con.execute("UPDATE users SET semester = ?, modules = ?, roles = ? WHERE id = ?", params![
                u.semester.to(),
                u.modules.iter().map(|m| m.as_str()).collect::<Vec<_>>().join(";"),
                u.roles.iter().map(|m| m.as_str()).collect::<Vec<_>>().join(";"),
                u.id.0
            ]).unwrap();
        } else {
            self.con.execute("INSERT INTO users (id, semester, modules, roles) VALUES (?, ?, ?, ?)", params![
                u.id.0,
                u.semester.to(),
                u.modules.iter().map(|m| m.as_str()).collect::<Vec<_>>().join(";"),
                u.roles.iter().map(|m| m.as_str()).collect::<Vec<_>>().join(";")
            ]).unwrap();
        }
    }

    pub fn get_user_max_level(&mut self, u: u64) -> Level {
        let roles = self.con.query_row("SELECT roles FROM users WHERE id = ?", [u], |row| Ok(
            row.get::<_, String>(0)?.split_terminator(";").map(|v| v.to_string()).collect::<Vec<_>>()
        )).unwrap();
        let roles = roles.iter().map(|r| self.get_role_by_name(r)).filter_map(|r| r).collect::<Vec<_>>();
        let mut lvl = Level::Base;
        for r in roles {
            if r.level > lvl {
                lvl = r.level;
            }
        }
        lvl
    }

    pub fn get_modules(&mut self) -> Vec<Module> {
        if let Ok(mut stmt) = self.con.prepare("SELECT name, semester, display FROM modules") {
            let query = stmt.query_map([], |row| Ok(Module {
                name: row.get(0)?,
                semester: Semester::from(row.get(1)?),
                display: row.get(2)?
            }));
            if let Ok(rows) = query {
                rows.filter_map(|r| r.ok()).collect()
            } else {
                Vec::new()
            }
        } else {
            Vec::new()
        }
    }

    pub fn get_module_by_name(&mut self, name: &str) -> Option<Module> {
        self.con.query_row("SELECT name, semester, display FROM modules WHERE name = ?", [name], |row| Ok(Module {
            name: row.get(0)?,
            semester: Semester::from(row.get(1)?),
            display: row.get(2)?
        })).ok()
    }

    pub fn write_module(&mut self, m: Module) {
        if self.con.query_row("SELECT 0 FROM modules WHERE name = ?", [&m.name], |_| Ok(())).is_ok() {
            self.con.execute("UPDATE modules SET semester = ?, display = ? WHERE name = ?", params![
                m.semester.to(),
                m.display,
                m.name
            ]).unwrap();
        } else {
            self.con.execute("INSERT INTO modules (name, semester, display) VALUES (?, ?, ?)", params![
                m.name,
                m.semester.to(),
                m.display
            ]).unwrap();
        }
    }

    pub fn delete_module_by_name(&mut self, name: &str) -> bool {
        self.con.execute("DELETE FROM modules WHERE name = ?", [name]).is_ok()
    }

    pub fn get_roles(&mut self) -> Vec<Role> {
        if let Ok(mut stmt) = self.con.prepare("SELECT name, colour, level, permissions FROM roles") {
            let query = stmt.query_map([], |row| Ok(Role {
                name: row.get(0)?,
                colour: Colour::new(row.get(1)?),
                level: Level::from(row.get(2)?),
                permissions: Permissions::from_bits(row.get(3)?).unwrap_or(Permissions::empty())
            }));
            if let Ok(rows) = query {
                rows.filter_map(|r| r.ok()).collect()
            } else {
                Vec::new()
            }
        } else {
            Vec::new()
        }
    }

    pub fn get_role_by_name(&mut self, name: &str) -> Option<Role> {
        self.con.query_row("SELECT name, colour, level, permissions FROM roles WHERE name = ?", [name], |row| Ok(Role {
            name: row.get(0)?,
            colour: Colour::new(row.get(1)?),
            level: Level::from(row.get(2)?),
            permissions: Permissions::from_bits(row.get(3)?).unwrap_or(Permissions::empty())
        })).ok()
    }

    pub fn write_role(&mut self, r: Role) {
        if self.con.query_row("SELECT 0 FROM roles WHERE name = ?", [&r.name], |_| Ok(())).is_ok() {
            self.con.execute("UPDATE roles SET colour = ?, level = ?, permissions = ? WHERE name = ?", params![
                r.colour.0,
                r.level.to(),
                r.permissions.bits,
                r.name
            ]).unwrap();
        } else {
            self.con.execute("INSERT INTO roles (name, colour, level, permissions) VALUES (?, ?, ?, ?)", params![
                r.name,
                r.colour.0,
                r.level.to(),
                r.permissions.bits
            ]).unwrap();
        }
    }

    pub fn delete_role_by_name(&mut self, name: &str) -> bool {
        self.con.execute("DELETE FROM roles WHERE name = ?", [name]).is_ok()
    }

    pub fn get_state(&mut self, k: &str) -> Option<String> {
        self.con.query_row("SELECT value FROM state WHERE key = ?", [k], |row| Ok(row.get(0)?)).ok()
    }

    pub fn set_state(&mut self, k: &str, v: String) {
        if self.con.query_row("SELECT 0 FROM state WHERE key = ?", [k], |_| Ok(())).is_ok() {
            self.con.execute("UPDATE state SET value = ? WHERE key = ?", params![v, k]).unwrap();
        } else {
            self.con.execute("INSERT INTO state (key, value) VALUES (?, ?)", params![k, v]).unwrap();
        }
    }

    pub fn get_date(&mut self) -> Date<Utc> {
        if let Some(date_string) = self.get_state("date") {
            Date::from_utc(NaiveDate::parse_from_str(&date_string, "%F").unwrap(), Utc)
        } else {
            let today = Utc::today();
            self.set_state("date", format!("{}", today.format("%F")));
            today
        }
    }

    pub fn update_date(&mut self) {
        let today = Utc::today();
        self.set_state("date", format!("{}", today.format("%F")));
    }
}

#[derive(Clone, Debug)]
pub struct User {
    pub id: UserId,
    pub semester: Semester,
    pub modules: HashSet<String>,
    pub roles: HashSet<String>
}


#[derive(Clone, Debug)]
pub struct Module {
    pub name: String,
    pub semester: Semester,
    pub display: String
}

#[derive(Clone, Debug)]
pub struct Role {
    pub name: String,
    pub colour: Colour,
    pub level: Level,
    pub permissions: Permissions
}

#[derive(Copy, Clone, Debug)]
pub enum Semester {
    None,
    Alumni,
    Sem(u32)
}

impl Semester {
    pub fn from(v: i32) -> Self {
        match v {
            1..=10 => Self::Sem(v as u32),
            11.. => Self::Alumni,
            ..1 => Self::None
        }
    }
    
    pub fn to(self) -> i32 {
        match self {
            Self::None => 0,
            Self::Alumni => 11,
            Self::Sem(s) => s as i32
        }
    }
}

#[derive(Copy, Clone, Debug, PartialEq, Eq, Ord)]
pub enum Level {
    Base,
    Mod,
    Admin
}

impl Level {
    pub fn from(v: i32) -> Self {
        match v {
            1 => Self::Mod,
            2 => Self::Admin,
            _ => Self::Base
        }
    }
    
    pub fn to(self) -> i32 {
        match self {
            Self::Base => 0,
            Self::Mod => 1,
            Self::Admin => 2
        }
    }
}

impl PartialOrd for Level {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        match (self, other) {
            (Level::Base, Level::Base) => Some(Ordering::Equal),
            (Level::Base, Level::Mod) => Some(Ordering::Less),
            (Level::Base, Level::Admin) => Some(Ordering::Less),
            (Level::Mod, Level::Base) => Some(Ordering::Greater),
            (Level::Mod, Level::Mod) => Some(Ordering::Equal),
            (Level::Mod, Level::Admin) => Some(Ordering::Less),
            (Level::Admin, Level::Base) => Some(Ordering::Greater),
            (Level::Admin, Level::Mod) => Some(Ordering::Greater),
            (Level::Admin, Level::Admin) => Some(Ordering::Equal)
        }
    }
}
